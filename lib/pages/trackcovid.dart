import 'package:covid/models/modelvaksin.dart';
import 'package:covid/others/source.dart';
import 'package:covid/pages/daftarvaksin.dart';
import 'package:covid/pages/maincovid.dart';
import 'package:covid/services/vaksin-services.dart';
import 'package:flutter/material.dart';

class TrackCovid extends StatefulWidget {
  const TrackCovid({Key? key}) : super(key: key);

  @override
  _TrackCovidState createState() => _TrackCovidState();
}

class _TrackCovidState extends State<TrackCovid> {
  List<ModelVaksin> _modelVaksin = [];
  VaksinServices _vaksinServices = VaksinServices();

  getDataVaksin() async {
    _modelVaksin = await _vaksinServices.getDataVaksin();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getDataVaksin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("COVID-19"),
        centerTitle: false,
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              height: 100,
              color: Colors.orange[100],
              padding: EdgeInsets.all(10),
              child: Text(
                Source.quotes,
                style: TextStyle(
                    color: Colors.orange[800],
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Text(
                "WorldWide",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
            ),
            MainCovid(),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Row(
                children: [
                  Text("Daftar Vaksin",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                  Spacer(),
                  IconButton(
                      onPressed: () =>
                          Navigator.of(context).popAndPushNamed('/addPage'),
                      icon: Icon(Icons.add_box_outlined))
                ],
              ),
            ),
            DaftarVakksin()
          ],
        ),
      ),
    );
  }
}

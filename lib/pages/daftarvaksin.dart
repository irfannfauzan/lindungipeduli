import 'package:covid/models/modelvaksin.dart';
import 'package:covid/services/vaksin-services.dart';
import 'package:flutter/material.dart';

class DaftarVakksin extends StatefulWidget {
  const DaftarVakksin({Key? key}) : super(key: key);

  @override
  _DaftarVakksinState createState() => _DaftarVakksinState();
}

class _DaftarVakksinState extends State<DaftarVakksin> {
  List<ModelVaksin> _modelVaksin = [];

  VaksinServices _vaksinServices = VaksinServices();

  getDataVaksin() async {
    _modelVaksin = await _vaksinServices.getDataVaksin();
    setState(() {});
  }

  @override
  void initState() {
    getDataVaksin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            final res = _modelVaksin[index];
            return GestureDetector(
              onTap: () => Navigator.of(context).popAndPushNamed('/updatePage',
                  arguments: [res.id, res.nama, res.alamat, res.umur]),
              child: ListTile(
                leading: Text(res.id),
                title: Text(res.nama),
                subtitle: Text(res.alamat),
                trailing: CircleAvatar(
                  backgroundImage: NetworkImage(res.avatar),
                ),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemCount: _modelVaksin.length),
    );
  }
}

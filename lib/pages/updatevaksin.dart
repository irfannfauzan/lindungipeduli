import 'package:covid/services/vaksin-services.dart';
import 'package:flutter/material.dart';

class UpdatePage extends StatefulWidget {
  const UpdatePage({Key? key}) : super(key: key);

  @override
  _UpdatePageState createState() => _UpdatePageState();
}

class _UpdatePageState extends State<UpdatePage> {
  VaksinServices _vaksinServices = VaksinServices();

  final _nama = TextEditingController();
  final _alamat = TextEditingController();
  final _umur = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as List<String>;
    if (args[1].isNotEmpty) {
      _nama.text = args[1];
    }
    if (args[2].isNotEmpty) {
      _alamat.text = args[2];
    }
    if (args[3].isNotEmpty) {
      _umur.text = args[3];
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Regist Vaksin"),
        centerTitle: true,
        leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed('/home');
            },
            icon: Icon(Icons.arrow_back_outlined)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextField(
              controller: _nama,
              decoration: InputDecoration(
                  hintText: "Nama",
                  icon: Icon(Icons.person),
                  border: OutlineInputBorder()),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: _umur,
              decoration: InputDecoration(
                  hintText: "Umur",
                  icon: Icon(Icons.person_add_alt),
                  border: OutlineInputBorder()),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: _alamat,
              decoration: InputDecoration(
                  hintText: "Alamat",
                  icon: Icon(Icons.home_outlined),
                  border: OutlineInputBorder()),
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 50,
              width: 200,
              child: ElevatedButton(
                  onPressed: () async {
                    bool response = await _vaksinServices.putData(
                        args[0], _nama.text, _alamat.text, _umur.text);
                    if (response) {
                      print('Berhasil Membuat Data');
                      Navigator.of(context).pushNamed('/home');
                    } else {
                      print('err');
                    }
                  },
                  child: Text("Update")),
            )
          ],
        ),
      ),
    );
  }
}

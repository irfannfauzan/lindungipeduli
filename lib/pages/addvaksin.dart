import 'dart:html';

import 'package:covid/others/source.dart';
import 'package:covid/services/vaksin-services.dart';
import 'package:flutter/material.dart';

class AddVaksin extends StatefulWidget {
  const AddVaksin({Key? key}) : super(key: key);

  @override
  _AddVaksinState createState() => _AddVaksinState();
}

class _AddVaksinState extends State<AddVaksin> {
  final _nama = TextEditingController();
  final _alamat = TextEditingController();
  final _umur = TextEditingController();

  VaksinServices _vaksinServices = VaksinServices();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Regist Vaksin"),
        centerTitle: true,
        leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed('/home');
            },
            icon: Icon(Icons.arrow_back_outlined)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextField(
              controller: _nama,
              decoration: InputDecoration(
                  hintText: "Nama",
                  icon: Icon(Icons.person),
                  border: OutlineInputBorder()),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: _umur,
              decoration: InputDecoration(
                  hintText: "Umur",
                  icon: Icon(Icons.person_add_alt),
                  border: OutlineInputBorder()),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: _alamat,
              decoration: InputDecoration(
                  hintText: "Alamat",
                  icon: Icon(Icons.home_outlined),
                  border: OutlineInputBorder()),
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 50,
              width: 200,
              child: ElevatedButton(
                  onPressed: () async {
                    bool response = await _vaksinServices.postData(
                        _nama.text, _alamat.text, _umur.text);
                    if (response) {
                      print('Berhasil Membuat Data');
                      Navigator.of(context).pushNamed('/home');
                    } else {
                      print('err');
                    }
                  },
                  child: Text("Simpan")),
            )
          ],
        ),
      ),
    );
  }
}

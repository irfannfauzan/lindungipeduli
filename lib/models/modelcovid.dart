class covidApi {
  final int positif;
  final int dirawat;
  final int sembuh;
  final int meninggal;

  covidApi(
      {required this.positif,
      required this.dirawat,
      required this.sembuh,
      required this.meninggal});

  factory covidApi.fromJson(final json) {
    return covidApi(
        positif: json['positif'],
        dirawat: json['dirawat'],
        sembuh: json['sembuh'],
        meninggal: json['meninggal']);
  }
}

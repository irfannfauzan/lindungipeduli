class ModelVaksin {
  final String id, nama, alamat, avatar, umur;

  ModelVaksin(
      {required this.id,
      required this.nama,
      required this.alamat,
      required this.avatar,
      required this.umur});

  factory ModelVaksin.fromJson(Map<String, dynamic> json) {
    return ModelVaksin(
        id: json['id'],
        nama: json['nama'],
        alamat: json['alamat'],
        avatar: json['avatar'],
        umur: json['umur']);
  }
}

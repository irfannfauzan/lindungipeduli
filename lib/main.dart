import 'package:covid/others/source.dart';
import 'package:covid/pages/addvaksin.dart';
import 'package:covid/pages/trackcovid.dart';
import 'package:covid/pages/updatevaksin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: primaryBlack,
      ),
      home: TrackCovid(),
      routes: {
        '/addPage': (context) => AddVaksin(),
        '/home': (context) => TrackCovid(),
        '/updatePage': (context) => UpdatePage(),
      },
    ),
  );
}

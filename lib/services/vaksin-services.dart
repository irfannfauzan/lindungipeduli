import 'dart:convert';

import 'package:covid/models/modelvaksin.dart';
import 'package:http/http.dart' as http;

class VaksinServices {
  final _url = ('https://615075caa706cd00179b7461.mockapi.io');

  Future getDataVaksin() async {
    try {
      final response = await http.get(Uri.parse(_url + '/covid19'));
      if (response.statusCode == 200) {
        print(response.body);
        Iterable modelVaksin = jsonDecode(response.body);
        List<ModelVaksin> _modelVaksin =
            modelVaksin.map((e) => ModelVaksin.fromJson(e)).toList();
        return _modelVaksin;
      } else {
        print('err');
      }
    } catch (_) {
      print(_.toString());
    }
  }

  Future postData(String nama, String alamat, String umur) async {
    try {
      final response = await http.post(Uri.parse(_url + '/covid19'), body: {
        "nama": nama,
        "alamat": alamat,
        "umur": umur,
      });
      if (response.statusCode == 201) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('err');
    }
  }

  Future putData(String id, String nama, String alamat, String umur) async {
    try {
      final response =
          await http.put(Uri.parse(_url + '/covid19/' + id), body: {
        "id": id,
        "nama": nama,
        "alamat": alamat,
        "umur": umur,
      });

      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (_) {
      print('err');
    }
  }
}

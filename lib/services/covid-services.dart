import 'dart:convert';
import 'package:covid/models/modelcovid.dart';
import 'package:http/http.dart' as http;

Future<covidApi> getDataCovid() async {
  final Uri _url =
      Uri.parse('https://apicovid19indonesia-v2.vercel.app/api/indonesia');
  final responses = await http.get(_url);
  if (responses.statusCode == 200) {
    final jsonCovid = jsonDecode(responses.body);
    return covidApi.fromJson(jsonCovid);
  } else {
    throw Exception();
  }
}
